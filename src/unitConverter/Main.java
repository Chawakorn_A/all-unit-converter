package unitConverter;
/**
 * Launcher of this project.
 * @author Chawakorn Aougsuk
 *
 */
public class Main {
	
	public static void main(String[] args) {
		UnitConverter a = new UnitConverter();
		ConverterUI gui = new ConverterUI(a);
		
	}
}

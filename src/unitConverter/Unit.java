package unitConverter;
/**
 * An interface that was created to define a unit of measurement.
 * @author Chawakorn Aougsuk
 *
 */
public interface Unit {

	/**
	 * A method to convert value from this unit to the other one, and will calculate the prefix too. 
	 * @param amount is the value you want to convert
	 * @param toUnit the unit you want to convert to
	 * @param fromPrefix the prefix of this unit
	 * @param toPrefix this prefix of the other unit
	 * @return double of value that have been calculate
	 */
	double convertTo (double amount , Unit toUnit, Prefix fromPrefix, Prefix toPrefix);
	/**
	 * Return the value of this enum.
	 * @return oalue of the enum
	 */
	double getValue();
	@Override
	String toString();
	/**
	 * Method for checking if this unit is metric or not.
	 * @return boolean 
	 */
	boolean isMetricUnit();
}

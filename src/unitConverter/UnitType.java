package unitConverter;

public enum UnitType {

		LENGTH ,
		AREA ,
		MASS ,
		PRESSURE ,
		VELOCITY,
		VOLUME,
		TIME; // this can include mass units, too
	
		
		
}

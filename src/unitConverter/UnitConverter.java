package unitConverter;
/**
 * A listener that 
 * @author User
 *
 */
public class UnitConverter {
	
	/**
	 * a caller that will call convertTo in Length.
	 * @param amount the amount of the unit.
	 * @param fromUnit the unit of the amount
	 * @param toUnit the unit that we want to converted to
	 * @return double of the unit that's been already computed
	 */
	public double convert ( double amount , Unit fromUnit , Unit toUnit , Prefix fromPrefix , Prefix toPrefix )
	{
		return fromUnit.convertTo(amount, toUnit, fromPrefix, toPrefix);
	}
	
	/**
	 * A method to get an array of things in enum.
	 * @return an array of element in enum
	 */
	public Unit[] getUnits()
	{
		return Length.values();
	}
	
	public Unit[] getUnitTypes( UnitType type )
	{
		if(type == UnitType.LENGTH)
			return Length.values();
		if(type == UnitType.AREA)
			return Area.values();
		if(type == UnitType.MASS)
			return Mass.values();
		if(type == UnitType.PRESSURE)
			return Pressure.values();
		if(type == UnitType.VELOCITY)
			return Velocity.values();
		if(type == UnitType.VOLUME)
			return Volume.values();
		if(type == UnitType.TIME)
			return Time.values();
		return null;
	}

}

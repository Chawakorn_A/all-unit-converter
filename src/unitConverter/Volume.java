package unitConverter;
/**
 * An Enum that do the calculating of volume type unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Volume implements Unit{
	C_Meter( "cubic meter",1.0, true ),
	LITER( "liter",0.001,true),
	BARREL( "barrel",0.158987294928),
	BUSHEL_UK( "bushel (UK)" , 0.03636872 ),
	BUSHEL_US( "bushel (US)", 0.03523907016688),
	C_YARD("cubic yard",0.764554857984),
	C_FOOT("cubic foot",0.028316846592),
	C_INCH("cubic inch",0.000016387064),
	FLD_DRAM("fluid dram",0.000003696691195313),
	FLD_OUNCE("fluid ounce",0.0000295735295625),
	FLD_OUNCE_UK("fluid ounce (UK)",0.0000284130625),
	GALLON("gallon",0.003785411784),
	GALLON_UK("gallon (UK)",0.00454609),
	GILL("gill",0.00011829411825),
	MINIM("minim",6.161151992187E-8),
	PECK("peck",0.00880976754187),
	PINT("pint",0.000473176473),
	PINT_UK("pint (UK)",0.00056826125),
	PINT_US("pint (US)",0.0005506104716575),
	QUART("quart",0.000946352946),
	QUART_UK("quart (UK)",0.0011365225),
	QUART_US("quart (US)",0.001101220942715)
	;
	private String name;
	private double value;
	private boolean isMetric;
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to 1 kilogram.
	 */
	private Volume( String name, double value, boolean isMetric) {
		this.name = name;
		this.value = value;
		this.isMetric = isMetric;
	}
	
	private Volume( String name, double value) {
		this.name = name;
		this.value = value;
		this.isMetric = false;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public boolean isMetricUnit() {
		// TODO Auto-generated method stub
		return this.isMetric;
	}
	@Override
	public double convertTo(double amount, Unit toUnit, Prefix fromPrefix,Prefix toPrefix) {
		return amount * Math.pow(fromPrefix.getValue(),3)* this.getValue() / ( toUnit.getValue()* Math.pow(toPrefix.getValue(),3) );
	}

}

package unitConverter;
/**
 * An Enum that do the calculating of presssure type unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Pressure implements Unit{
	PASCAL("pascal",1,true),
	BAR( "bar",100000,true ),
	ATMOSPHERE( "atmosphere (atm)",10325 ),
	KG_PER_SQCM( "kilgram per sq. cm",98066.5 ),
	KG_PER_SQM("kilgram per sq. m",9.80665),
	POUNDS_PER_sqft( "Pound per square foot",47.88020833333 ),
	POUNDS_PER_sqi( "Pound per square inch",6894.75 ),
	TORR( "torr",133.3223684211 );
	
	

	private String name;
	private double value;
	private boolean isMetric;
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to 1 kilogram.
	 */
	private Pressure( String name, double value, boolean isMetric) {
		this.name = name;
		this.value = value;
		this.isMetric = isMetric;
	}
	
	private Pressure( String name, double value) {
		this.name = name;
		this.value = value;
		this.isMetric = false;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public boolean isMetricUnit() {
		// TODO Auto-generated method stub
		return this.isMetric;
	}
	@Override
	public double convertTo(double amount, Unit toUnit, Prefix fromPrefix,Prefix toPrefix) {
		return amount * fromPrefix.getValue()*this.getValue() / ( toPrefix.getValue()* toUnit.getValue() );
	}

}

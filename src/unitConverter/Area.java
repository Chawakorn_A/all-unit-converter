package unitConverter;

/**
 * An Enum that do the calculating of area type unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Area implements Unit{
	SQUARE_METER( "square meter",1.0, true ),
	ARE( "are",100),
	BARN( "barn",1E-28),
	HECTARE( "hectare" , 10000 ),
	HOMESTEAD( "homestead", 647497.027584),
	ROOD("rood",1011.7141056),
	SQUARE_FEET( "square feet",0.09290304),
	SQUARE_MILE( "square  mile",2589988.110336),
	SQUARE_ROD( "square rod",25.29285264 ),
	SQUARE_YARD("square yard",0.83612736),
	TOWNSHIP("township",93239571.9721),
	ACRE( "acre",4046.85642)
	;

	private String name;
	private double value;
	private boolean isMetric;
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to 1 square meter.
	 */
	private Area( String name, double value) {
		this.name = name;
		this.value = value;
		this.isMetric = false;
	}

	private Area( String name, double value , boolean isMetric) {
		this.name = name;
		this.value = value;
		this.isMetric = isMetric;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public boolean isMetricUnit() {
		// TODO Auto-generated method stub
		return this.isMetric;
	}
	@Override
	public double convertTo(double amount, Unit toUnit, Prefix fromPrefix,Prefix toPrefix) {
		return amount * Math.pow(fromPrefix.getValue(),2)* this.getValue() / ( toUnit.getValue()* Math.pow(toPrefix.getValue(),2) );
	}

}

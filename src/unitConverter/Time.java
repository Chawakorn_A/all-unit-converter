package unitConverter;
/**
 * An Enum that do the calculating of time type unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Time implements Unit{
	SECOND("second",1,true),
	MINUTE("minute",60),
	HOUR("hour",3600),
	DAY("day",86400),
	WEEK("week",604800),
	MONTH_28("month 28",86400*28),
	MONTH_29("month 29",86400*29),
	MONTH_30("month 30",86400*30),
	MONTH_31("month 31",86400*31),
	FORTNIGHT("fortnight",1209600),
	YEAR("year",31536000),
	LEAP_YEAR("leap year",3153600+24*86400),
	TROPICAL_YEAR("tropical year",31556925.9936),
	GREGOIRAN_YEAR("Gregorian year",31556952),
	JULIAN_YEAR("Julian year",31557600.0),
	DECADE("decade",315360000.0),
	CENTURY("century",315360000.0),
	MILLENNIUM("millennium",3153600000.0)
	;
	
	

	private String name;
	private double value;
	private boolean isMetric;
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to 1 kilogram.
	 */
	private Time( String name, double value, boolean isMetric) {
		this.name = name;
		this.value = value;
		this.isMetric = isMetric;
	}
	
	private Time( String name, double value) {
		this.name = name;
		this.value = value;
		this.isMetric = false;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public boolean isMetricUnit() {
		// TODO Auto-generated method stub
		return this.isMetric;
	}
	@Override
	public double convertTo(double amount, Unit toUnit, Prefix fromPrefix,Prefix toPrefix) {
		return amount * fromPrefix.getValue()*this.getValue() / ( toPrefix.getValue()* toUnit.getValue() );
	}
}

package unitConverter;
/**
 * An Enum that do contain prefix for metric unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Prefix {
	YOTTA( "yotta/Y",1E+24 ),
	ZETTA( "zetta/Z",1E+21 ),
	EXA( "exa/E",1E+18 ),
	PETA( "peta/P",1E+15 ),
	TERA( "tera/T",1E+12 ),
	GIGA( "giga/G",1E+9 ),
	MEGA( "mega/M",1E+6 ),
	KILO( "kilo/k",1E+3 ),
	HECTO( "hecto/h",1E+2 ),
	DECA( "deca/da",1E+1 ),
	NONE( "-",1E+0 ),
	DECI( "deci/d",1E-1),
	CENTI( "centi/c",1E-2),
	MILLI( "milli/m",1E-3),
	MICRO( "micro/μ",1E-6 ),
	NANO( "nano/n",1E-9 ),
	PICO( "pico/p",1E-12 ),
	FEMTO( "femto/f",1E-15 ),
	ATTO( "atto/a",1E-18 ),
	ZEPTO( "zepto/z",1E-21 ),
	YOCTO( "yocto/y",1E-24 );
	
	
	private String name;
	private double value;
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to none-prefix.
	 */
	private Prefix( String name, double value) {
		this.name = name;
		this.value = value;
	}
	public double getValue() {
		return this.value;
	}
	public String toString() {
		return this.name;
	}
}

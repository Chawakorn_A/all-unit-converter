package unitConverter;
/**
 * An Enum that do the calculating of velocity type unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Velocity implements Unit{
	
	M_Per_SEC("meter per second",1,true),
	M_Per_MIN("meter per minuted",0.016666667,true),
	M_Per_HOUR("meter per hour",0.000277777778,true),
	FT_Per_SEC("foot per second",0.3048),
	KNOT("knot",0.51444444),
	MACH("mach(at standard atm.)",343),
	MILE_PER_HOUR("mile per hour",0.44704),
	SPEED_OF_LIGHT("speed of light",299792458),
	YARD_Per_SEC("yard per second",0.9144)
	;
	
	private String name;
	private double value;
	private boolean isMetric; 
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to 1 meter.
	 */
	private Velocity( String name, double value, boolean isMetric) {
		this.name = name;
		this.value = value;
		this.isMetric = isMetric;
	}
	
	private Velocity( String name, double value) {
		this.name = name;
		this.value = value;
		this.isMetric = false;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public boolean isMetricUnit() {
		return this.isMetric;
	}

	@Override
	public double convertTo(double amount, Unit toUnit, Prefix fromPrefix,Prefix toPrefix) {
		// TODO Auto-generated method stub
		return amount * fromPrefix.getValue() * this.getValue() / ( toPrefix.getValue() * toUnit.getValue() );
	}

}

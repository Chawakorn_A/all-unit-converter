package unitConverter;
/**
 * An Enum that do the calculating of mass type unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Mass implements Unit {
	GRAM( "gram",1,true ),
	CARAT( "carat",0.2,true ),
	POUND( "pound",453 ),
	TONNE( "tonne",1000000.0,true ),
	LONG_TON("long ton",1016046.9088),
	SHORT_TON( "short ton",90718.74 ),
	AMU("atomic mass unit",1.6605402E-24),
	CENTAL("cental",45359.237),
	DRAM("dram",1.771845195312),
	GRAIN("grain",0.06479891),
	HUNDREDWEIGHT("hundredweight",50802.34544),
	NEWTON("newton",101.9716212978),
	OUNCE("ounce",28.349523125),
	PENNYWEIGHT("pennyweight",1.55517384),
	QUARRTER("quarter",12700.58636),
	STONE("stone",6350.29318),
	TROY_OUNCE("troy ounce",31.1034768)
	;
	

	private String name;
	private double value;
	private boolean isMetric;
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to 1 kilogram.
	 */
	private Mass( String name, double value, boolean isMetric) {
		this.name = name;
		this.value = value;
		this.isMetric = isMetric;
	}
	
	private Mass( String name, double value) {
		this.name = name;
		this.value = value;
		this.isMetric = false;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public boolean isMetricUnit() {
		// TODO Auto-generated method stub
		return this.isMetric;
	}
	@Override
	public double convertTo(double amount, Unit toUnit, Prefix fromPrefix,Prefix toPrefix) {
		return amount * fromPrefix.getValue()*this.getValue() / ( toPrefix.getValue()* toUnit.getValue() );
	}

}

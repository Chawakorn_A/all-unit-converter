package unitConverter;
/**
 * An Enum that do the calculating of length type unit.
 * @author Chawakorn Aougsuk
 *
 */
public enum Length implements Unit{

	METER( "meter",1.0,true ),
	LEAGUE( "league",1609.344*3,false),
	MILE( "mile",1609.344,false),
	FOOT( "foot",0.3048,false ),
	YARD( "yard",0.9144,false ),
	INCH( "inch",0.0254,false ),
	LINE( "line",0.0254/12,false ),
	THOU( "thou/mil",0.0254/1000,false ),
	Light_YEAR( "light year", 9.4605284E+15,false );
	
	private String name;
	private double value;
	private boolean isMetric; 
	
	/**
	 * Constructor of the Enum.
	 * @param name is the name of this unit
	 * @param value is the size of this unit compare to 1 meter.
	 */
	private Length( String name, double value, boolean isMetric) {
		this.name = name;
		this.value = value;
		this.isMetric = isMetric;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public boolean isMetricUnit() {
		return this.isMetric;
	}

	@Override
	public double convertTo(double amount, Unit toUnit, Prefix fromPrefix,Prefix toPrefix) {
		// TODO Auto-generated method stub
		return amount * fromPrefix.getValue() * this.getValue() / ( toPrefix.getValue() * toUnit.getValue() );
	}
	
	

}

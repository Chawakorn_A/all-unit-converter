package unitConverter;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

/**
 * A GUI of the UnitConverter class
 * @author Chawakorn Aougsuk
 *
 */
public class ConverterUI extends JFrame
{


	private JMenuBar menuBar;
	private JButton convertButton;
	private UnitConverter unitconverter;
	private JComboBox<Unit> unit1ComboBox;
	private JComboBox<Unit> unit2ComboBox;
	private JComboBox<Unit> inputComboBox;
	private JComboBox<Unit> outputComboBox;
	private JTextField inputTextField;
	private JTextField outputTextField;
	private JTextField textField1;
	private JTextField textField2;
	private JButton clearButton;
	private Container contents;
	private UnitType utype;
	private String text1Input = "";
	private String text2Input = "";
	private JPanel inputPanel;
	private JPanel panel_1;
	private JPanel outputPanel;
	private JPanel panel_3;
	private JComboBox<Prefix> prefixComboBox1;
	private JComboBox<Prefix> prefixComboBox2;
	private ItemListener comboBoxListener;

	/**
	 * Constructor of this class
	 * @param uc is a UnitCoverter that was passed to this class
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Unit Converter");
		this.setDefaultCloseOperation( EXIT_ON_CLOSE );
		this.utype = UnitType.LENGTH;
		initComponents( );
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {

		JFrame frame = new JFrame("MenuSample Example");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menuBar = new JMenuBar();
		JMenu task1 = new JMenu("Unit Type");
		task1.setMnemonic(KeyEvent.VK_F);
		menuBar.add(task1);

		JMenuItem menuItem1 = new JMenuItem("Length" , KeyEvent.VK_L);
		JMenuItem menuItem2 = new JMenuItem("Area" , KeyEvent.VK_A);
		JMenuItem menuItem3 = new JMenuItem("Mass" , KeyEvent.VK_M);
		JMenuItem menuItem4 = new JMenuItem("Pressure" , KeyEvent.VK_P);
		JMenuItem menuItem5 = new JMenuItem("Velocity" , KeyEvent.VK_V);
		JMenuItem menuItem6 = new JMenuItem("Volume" , KeyEvent.VK_O);
		JMenuItem menuItem7 = new JMenuItem("Time" , KeyEvent.VK_T);
		
		JMenuItem menuItemExit = new JMenuItem("Exit" , KeyEvent.VK_X);
		
		task1.add(menuItem1);
		menuItem1.addActionListener(new setLength());
		
		task1.add(menuItem2);
		menuItem2.addActionListener(new setArea());
		
		task1.add(menuItem3);
		menuItem3.addActionListener(new setWeight());
		
		task1.add(menuItem4);
		menuItem4.addActionListener(new setPressure());
		
		task1.add(menuItem5);
		menuItem5.addActionListener(new setVelocity());
		
		task1.add(menuItem6);
		menuItem6.addActionListener(new setVolume());
		
		task1.add(menuItem7);
		menuItem7.addActionListener(new setTime());
		
		task1.addSeparator();
		
		task1.add(menuItemExit);
		menuItemExit.addActionListener(new exitApp());

		this.setJMenuBar(menuBar);
		
		ActionListener listener1 = new ConvertButtonListener( );
		ActionListener listener3 = new ClearButtonListener( );
		comboBoxListener = new comboBoxHandler( );

		contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		clearButton = new JButton("Clear");
		contents.setLayout( layout );
		
		panel_1 = new JPanel();
		getContentPane().add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		inputPanel = new JPanel();
		panel_1.add(inputPanel);
		inputPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
				textField1 = new JTextField(20);
				inputPanel.add(textField1);
						
						prefixComboBox1 = new JComboBox<Prefix>();
						inputPanel.add(prefixComboBox1);
				
						unit1ComboBox = new JComboBox<Unit>( );
						inputPanel.add(unit1ComboBox);
						
						panel_3 = new JPanel();
						panel_1.add(panel_3);
						JLabel equalLabel = new JLabel("=");
						panel_3.add(equalLabel);
						
						outputPanel = new JPanel();
						panel_1.add(outputPanel);
						outputPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
						textField2 = new JTextField(20);
						outputPanel.add(textField2);
						textField2.setEditable(false);
						
						prefixComboBox2 = new JComboBox<Prefix>();
						outputPanel.add(prefixComboBox2);
						unit2ComboBox = new JComboBox<Unit>( );
						outputPanel.add(unit2ComboBox);
						textField2.addActionListener(listener1);
						textField1.addActionListener(listener1);
		convertButton = new JButton("Convert");
		getContentPane().add(convertButton);
		convertButton.addActionListener( listener1 );
		contents.add(clearButton);

		this.setPrefixComboBox();
		clearTextField();
		setComboBox();
		clearButton.addActionListener( listener3 );
		this.setVisible(true);
		this.pack(); // resize the Frame to match size of components
	}

	public void setPrefixComboBox(){
		Prefix[] prefix = Prefix.values();
		for( Prefix u : prefix ) prefixComboBox1.addItem( u );
		for( Prefix u : prefix ) prefixComboBox2.addItem( u );
		this.setPrefixAtNone();
		this.pack();
	}
	
	public void setPrefixAtNone(){
		prefixComboBox1.setSelectedItem(Prefix.NONE);
		prefixComboBox2.setSelectedItem(Prefix.NONE);
	}
	
	public void setComboBox() {
		clearTextField();
    	unit1ComboBox.removeAllItems();
    	unit2ComboBox.removeAllItems();
    	
    	prefixComboBox1.setEnabled(true);
    	prefixComboBox2.setEnabled(true);
    	
    	Unit[] unit = unitconverter.getUnitTypes(utype);
		for( Unit u : unit ) unit1ComboBox.addItem( u );
		for( Unit u : unit ) unit2ComboBox.addItem( u );
		
		unit1ComboBox.addItemListener(comboBoxListener);
		unit2ComboBox.addItemListener(comboBoxListener);
		this.pack();
		
	}
	
	public void removeHandler() {
		unit1ComboBox.removeItemListener(comboBoxListener);
    	unit2ComboBox.removeItemListener(comboBoxListener);
	}
	
	protected void clearTextField()
	{
		textField1.setText("");
		textField2.setForeground(Color.black);
		textField2.setText( "" );
		textField2.setForeground(Color.black);
	}
	
	/**
	 * A hadler for computing the covert.
	 * @author Chawakorn Aougsuk
	 *
	 */
	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {


			String s1 = textField1.getText().trim();
			String s2 = textField2.getText().trim();
				inputTextField = textField1;
				outputTextField = textField2;
				inputComboBox = unit1ComboBox;
				outputComboBox = unit2ComboBox;
			text1Input = s1;
			text2Input = s2;
			try{
				inputTextField.setForeground(Color.BLACK);
				if ( s1.length() > 0 ) {
					double value = Double.valueOf( s1 );
					Unit fromUnit = (Unit) inputComboBox.getSelectedItem();
					Unit toUnit = (Unit) outputComboBox.getSelectedItem();
					Prefix fromPrefix = (Prefix) prefixComboBox1.getSelectedItem();
					Prefix toPrefix = (Prefix) prefixComboBox2.getSelectedItem();
					outputTextField.setText( ""+unitconverter.convert(value, fromUnit, toUnit , fromPrefix , toPrefix) );
					
					s1 = textField1.getText().trim();
					s2 = textField2.getText().trim();
					text1Input = s1;
					text2Input = s2;
				}		
				else
					throw new NumberFormatException();
			}
			catch (NumberFormatException a){
				inputTextField.setForeground(Color.RED);
				JOptionPane.showMessageDialog(contents,"Invalid Input","Error",JOptionPane.ERROR_MESSAGE);
			}
			

		}
	} // end of the inner class for ConvertButtonListener

	/**
	 * A hadler for clearing the textfield.
	 * @author User
	 *
	 */
	class ClearButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			clearTextField();
		}
	} // end of the inner class for ConvertButtonListener
	
	
	static class exitApp implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            System.exit(0);
        }
    }
	
	class setLength implements ActionListener
    {
        

		public void actionPerformed(ActionEvent e)
        {
			utype = UnitType.LENGTH;
			removeHandler();
			setComboBox();
        }
    }
	
	class setWeight implements ActionListener
    {
        

		public void actionPerformed(ActionEvent e)
        {
			utype = UnitType.MASS;
			removeHandler();
			setComboBox();
        }
    }
	
	class setArea implements ActionListener
    {
        

		public void actionPerformed(ActionEvent e)
        {
			utype = UnitType.AREA;
			removeHandler();
			setComboBox();

        }
    }
	
	class setPressure implements ActionListener
    {
        

		public void actionPerformed(ActionEvent e)
        {
			utype = UnitType.PRESSURE;
			removeHandler();
			setComboBox();
        }
    }
	
	class setVelocity implements ActionListener
    {
		public void actionPerformed(ActionEvent e)
        {
			utype = UnitType.VELOCITY;
			removeHandler();
			setComboBox();
        }
    }
	
	class setVolume implements ActionListener
    {
		public void actionPerformed(ActionEvent e)
        {
			utype = UnitType.VOLUME;
			removeHandler();
			setComboBox();
        }
    }
	
	class setTime implements ActionListener
    {
        
		public void actionPerformed(ActionEvent e)
        {
			utype = UnitType.TIME;
			removeHandler();
			setComboBox();
        }
    }
	
	class comboBoxHandler implements ItemListener
    {

		@Override
		public void itemStateChanged(ItemEvent arg0) {
			Unit fromUnit = (Unit) unit1ComboBox.getSelectedItem();
			Unit toUnit = (Unit) unit2ComboBox.getSelectedItem();
			setPrefixAtNone();
			
			if(fromUnit.isMetricUnit()) prefixComboBox1.setEnabled(true);
			else prefixComboBox1.setEnabled(false);
			
			if(toUnit.isMetricUnit()) prefixComboBox2.setEnabled(true);
			else prefixComboBox2.setEnabled(false);

			
		}
    }
}




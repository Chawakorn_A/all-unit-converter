All Unit Converter 
==============

Project Proposal
--------------

Chawakorn Aougsuk 5710545015

Description
--------------

this is OOP2 PA6, a.k.a. final project, by me. This is a software that convert the measurement from one unit to the other unit which will help a lot of people with physics calculation or to understand the different between metrics, imperials, etc. unit

Feature
--------------

I plan to make this software be able to handle basic physics unit, like km/h to mph, and also be able to use the prefix too.